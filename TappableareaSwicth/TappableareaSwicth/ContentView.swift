//
//  ContentView.swift
//  TappableareaSwicth
//
//  Created by student on 2/5/20.
//  Copyright © 2020 student. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State private var scale:CGFloat = 1.0
    var body: some View {
        VStack{
//            Image("ichigo").resizable().frame(width: 100, height: 100)
//            Spacer().frame(height: 10)
//            Text("ichigo")
//        }
//    .contentShape(Rectangle())
//        .onTapGesture {
//            print("Show details for ichigo")
//        }
            Image("ichigo")
            .resizable()
                .frame(width: 150, height: 150)
            .scaleEffect(scale)
//                .gesture(TapGesture()
//                    .onEnded{ _ in
//                    self.scale += 0.1
//                }
//                .gesture(LongPressGesture(minimumDuration: 2).onEnded{ _ in
//                    print("Pressed!")
//
//                    }
//            )
            .gesture(DragGesture(minimumDistance: 50)
                .onEnded{ _ in
                    print("Dragged!")
            }
            )
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
