//
//  ContentView.swift
//  DatepickerSwicth
//
//  Created by student on 2/5/20.
//  Copyright © 2020 student. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var dateFormatter:DateFormatter{
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        return formatter
    }
    @State private var birthDate = Date()
    var body: some View {
        VStack{
            DatePicker(selection: $birthDate, in: ...Date(), displayedComponents: .date){
                Text("Select a date")
            }
            Text("Date is \(birthDate,formatter: dateFormatter)")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
