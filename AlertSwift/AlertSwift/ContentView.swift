//
//  ContentView.swift
//  AlertSwift
//
//  Created by student on 3/4/20.
//  Copyright © 2020 student. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State private var showingAlert = false
    var body: some View {
        Button(action:{
            self.showingAlert = true
        }){
            Text("Show Alert")
        }
        .alert(isPresented:$showingAlert) {
            Alert(title:Text("Are you sure you want to delete this?"), message:Text("There is no undo"), primaryButton: .destructive(Text("Delete")){
                print("Deleting...")
                }, secondaryButton: .cancel())
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
