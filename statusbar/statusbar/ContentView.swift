//
//  ContentView.swift
//  statusbar
//
//  Created by student on 3/4/20.
//  Copyright © 2020 student. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State var hideStatusBar = false
    var body: some View {
        Button("Toggle Status Bar") {
            withAnimation {
                self.hideStatusBar.toggle()
            }
        }
    .statusBar(hidden: hideStatusBar)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
