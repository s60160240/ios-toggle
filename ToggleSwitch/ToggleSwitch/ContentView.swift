//
//  ContentView.swift
//  ToggleSwitch
//
//  Created by student on 2/5/20.
//  Copyright © 2020 student. All rights reserved.
//

import SwiftUI


struct ContentView: View {
    @State private var showGreeting = true
    @State private var showDetails = false
    @State private var name:String = "bew"
    var body: some View {
        VStack{
            Toggle(isOn:$showGreeting){
                Text("Show welcome message")
            }.padding()

            if showGreeting {
                Text("Hello world!")
            }
        }

        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
