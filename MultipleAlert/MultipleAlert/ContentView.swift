//
//  ContentView.swift
//  MultipleAlert
//
//  Created by student on 3/4/20.
//  Copyright © 2020 student. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State private var showingAlert1 = false
    @State private var showingAlert2 = false
    var body: some View {
        VStack {
            Button("show 1"){
                self.showingAlert1 = true
            }
            .alert(isPresented:$showingAlert1){
                Alert(title:Text("One"), message: nil, dismissButton: .cancel())
            }
            
            Button("show 2"){
                self.showingAlert2 = true
            }
            .alert(isPresented:$showingAlert2){
                Alert(title:Text("Two"), message: nil, dismissButton: .cancel())
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
