//
//  ContentView.swift
//  pushnavigationview
//
//  Created by student on 3/4/20.
//  Copyright © 2020 student. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView {
            VStack {
                NavigationLink(destination: DetailView()){
                    Text("Show Detail View")
                }.navigationBarTitle("Navigation")
            }
        }
    }
}

struct DetailView:View {
    var body: some View {
        Text("This is the detail view")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
