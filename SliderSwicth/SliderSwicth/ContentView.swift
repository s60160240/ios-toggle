//
//  ContentView.swift
//  SliderSwicth
//
//  Created by student on 2/5/20.
//  Copyright © 2020 student. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State private var celsius:Double = 0
    var body: some View {
        VStack{
            Slider(value:$celsius,in:-100...100,step: 0.1).padding(.horizontal)
            Text("\(celsius) Celsius is \(celsius*9/5+32)Fahrenheit")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
