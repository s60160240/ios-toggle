//
//  ContentView.swift
//  EnvironmentobjectSwicth
//
//  Created by student on 2/12/20.
//  Copyright © 2020 student. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @EnvironmentObject var settings: UserSettings
    var body: some View {
        NavigationView{
            VStack{
                Button(action: {
                    self.settings.score += 1
                }){
                    Text("Increase Score")
                }.padding(.bottom, 50.0)
                NavigationLink(destination: DetailView()){
                    Text("Show Detail View")
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct DetailView:View {
    @EnvironmentObject var settings: UserSettings
    var body:some View{
        Text("Score: \(settings.score)")
    }
}

class UserSettings: ObservableObject {
    @Published var score = 0
}
