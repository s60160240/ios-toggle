//
//  ContentView.swift
//  DisabletapsSwicth
//
//  Created by student on 2/12/20.
//  Copyright © 2020 student. All rights reserved.
//

import SwiftUI

var colors = ["Red","Green","Blue","Tartan"]

struct ContentView: View {
    @State private var selectedColor=0
    var body: some View {
        ZStack{
            Button("Tap Me"){
                print("Button was tapped")
            }.frame(width: 100, height: 100)
            .background(Color.white)
            Rectangle()
                .fill(Color.red.opacity(0.2))
                .frame(width:300,height: 300)
            .clipShape(Circle())
            .allowsHitTesting(false)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
