//
//  ContentView.swift
//  TextfieldSwicth
//
//  Created by student on 2/5/20.
//  Copyright © 2020 student. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State private var name:String = "bew"
    var body: some View {
        VStack
            {
                TextField("Enter your name", text: $name).padding(.horizontal)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .disableAutocorrection(true)
                Text("Hello, \(name)!")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
