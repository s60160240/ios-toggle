//
//  ContentView.swift
//  DoubletapgestureSwicth
//
//  Created by student on 2/5/20.
//  Copyright © 2020 student. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack{
            Text("Tap me!")
                .padding(.bottom, 100.0)
                .onTapGesture {
                    print("Tapped!")
            }
            
            Text("Double Tap me!")
                .onTapGesture(count :2) {
                    print("Double Tapped!")
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
