//
//  ContentView.swift
//  UseSwicth
//
//  Created by student on 2/12/20.
//  Copyright © 2020 student. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var settings = UserSettings()
    var body: some View {
        VStack{
            Text("Your score is \(settings.score)")
            Button(action:{
                self.settings.score += 1
            }){
                Text("Increase Score")
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

class UserSettings: ObservableObject{
    @Published var score = 0
}
