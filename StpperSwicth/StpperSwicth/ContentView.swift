//
//  ContentView.swift
//  StpperSwicth
//
//  Created by student on 2/5/20.
//  Copyright © 2020 student. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State private var age = 18
    var body: some View {
//        VStack{
//            Stepper("Enter your age", value: $age, in: 0...130)
//            Text("Your age is \(age)")
//        }
        VStack{
            Stepper("Enter your age", onIncrement: {
                self.age += 1
                print("Adding to age")
            }, onDecrement: {
                self.age -= 1
                print("Subtracting from age")
            })
            Text("Your age is \(age)")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
