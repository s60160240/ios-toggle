//
//  ContentView.swift
//  cmManu
//
//  Created by student on 3/4/20.
//  Copyright © 2020 student. All rights reserved.
//

import SwiftUI

struct ContentView: View {
       var body: some View {
        Text("Options")
            .contextMenu {
                Button(action:{
                    //change country setting
                }){
                    Text("Choose Country")
                    Image(systemName: "globe")
                }
                
                Button(action:{
                    //enable geolocation
                }){
                    Text("Detect Location")
                    Image(systemName: "location.circle")
                }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
