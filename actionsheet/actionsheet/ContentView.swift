//
//  ContentView.swift
//  actionsheet
//
//  Created by student on 3/4/20.
//  Copyright © 2020 student. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State private var showingAlert = false
    var body: some View {
        Button(action:{
            self.showingAlert = true
        }){
            Text("Show Alert")
        }
        .actionSheet(isPresented:$showingAlert) {
            ActionSheet(title:Text("What do you want to do?"), message:Text("There's only one choice..."), buttons: [.default(Text("Dismiss Action Sheet")), .default(Text("You Action Sheet")), .default(Text("Dm Ai S"))] )
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
