//
//  ContentView.swift
//  calculater
//
//  Created by student on 2/5/20.
//  Copyright © 2020 student. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State private var firstNumber = 0.0
    @State private var secondNumber = 0.0
    @State private var minusNumber = 0.0
    @State private var result = 0.0
    @State private var operand = ""
    @State private var calculatorText = "0"
    @State private var isTypingNumber = false
    @State private var isTypingDot = true
    //@State private var isTapped:Bool = false
    var body: some View {
        VStack {Color(red: 0.0, green: 0.0, blue: 0.0)
            
            TextField("0", text: $calculatorText)
                .padding(.trailing)
                .font(.system(size: 100))
                .foregroundColor(Color.white)
            .multilineTextAlignment(.trailing)
                
            HStack(alignment: .center, spacing: 0.0) {
                createCalcAC().padding(.all, 30.0).frame(width: 90.0).background(Color.gray)
                createCalcMinus().padding(.all, 30.0).frame(width: 90.0).background(Color.gray)
                createCalcPercent().padding(.all, 30.0).frame(width: 90.0).background(Color.gray)
                createCalcOperand("/").padding(.all, 30.0).frame(width: 90.0).background( Color.orange)
            }
            .padding(.trailing)
            
            
            HStack(spacing: 0.0) {
                createCalcDigit("7").padding(.all, 30.0).frame(width: 90.0).background(Color(UIColor.darkGray))
                createCalcDigit("8").padding(.all, 30.0).frame(width: 90.0).background(Color(UIColor.darkGray))
                createCalcDigit("9").padding(.all, 30.0).frame(width: 90.0).background(Color(UIColor.darkGray))
                createCalcOperand("*").padding(.all, 30.0).frame(width: 90.0).background( Color.orange)
            }
            .padding(.trailing)
            
            HStack(spacing: 0.0) {
                createCalcDigit("4").padding(.all, 30.0).frame(width: 90.0).background(Color(UIColor.darkGray))
                createCalcDigit("5").padding(.all, 30.0).frame(width: 90.0).background(Color(UIColor.darkGray))
                createCalcDigit("6").padding(.all, 30.0).frame(width: 90.0).background(Color(UIColor.darkGray))
                createCalcOperand("-").padding(.all, 30.0).frame(width: 90.0).background( Color.orange)
            }
            .padding(.trailing)
            
            HStack(spacing: 0.0) {
                createCalcDigit("1").padding(.all, 30.0).frame(width: 90.0).background(Color(UIColor.darkGray))
                createCalcDigit("2").padding(.all, 30.0).frame(width: 90.0).background(Color(UIColor.darkGray))
                createCalcDigit("3").padding(.all, 30.0).frame(width: 90.0).background(Color(UIColor.darkGray))
                createCalcOperand("+").padding(.all, 30.0).frame(width: 90.0).background( Color.orange)
            }
            .padding(.trailing)
            
            HStack(spacing: 0.0) {
                createCalcDigit("0").padding(.all, 30.0).frame(width: 182.0).background(Color(UIColor.darkGray))
                createCalcDot().padding(.all, 30.0).frame(width: 90.0).background(Color(UIColor.darkGray))
                createCalcEqual().padding(.all, 30.0).frame(width: 90.0).background(Color.orange)
            }
            .padding(.trailing)
        }.background(Color.black)
    }
    
    private func createCalcDigit(_ number: String) -> some View {
        return Button(action: {
            self.digitTapped(number)
        }) {
            (Text(number)).frame(minWidth: 100, maxWidth: .infinity, minHeight: 20, maxHeight: 20, alignment: .center)
                .font(.system(size: 30))
                .foregroundColor(Color.white)
        }
    }
    private func createCalcDot() -> some View {
        return Button(action: {
            self.dotTapped()
        }) {
            (Text(".")).frame(minWidth: 100, maxWidth: .infinity, minHeight: 20, maxHeight: 20, alignment: .center)
                .font(.system(size: 30))
                .foregroundColor(Color.white)
        }
    }
    private func createCalcMinus() -> some View {
        return Button(action: {
            self.minusTapped()
        }) {
            (Text("+/-")).frame(minWidth: 100, maxWidth: .infinity, minHeight: 20, maxHeight: 20, alignment: .center).font(.system(size: 30))
                .foregroundColor(Color.white)
        }
    }
    private func createCalcPercent() -> some View {
        return Button(action: {
            self.percentTapped()
        }) {
            (Text("%")).frame(minWidth: 100, maxWidth: .infinity, minHeight: 20, maxHeight: 20, alignment: .center).font(.system(size: 30)) .foregroundColor(Color.white)
        }
    }
    private func createCalcOperand(_ operand: String) -> some View {
        return Button(action: {
            self.operandTapped(operand)
            //self.isTapped = true
        }) {
            (Text(operand)).frame(minWidth: 100, maxWidth: .infinity, minHeight: 20, maxHeight: 20, alignment: .center).font(.system(size: 30))
                .foregroundColor(Color.white)
        }
    }
    private func createCalcEqual() -> some View {
        return Button(action: {
            self.calculate()
        }) {
            (Text("="))
                .frame(minWidth: 100, maxWidth: .infinity, minHeight: 20, maxHeight: 20, alignment: .center)
                .font(.system(size: 30))                .foregroundColor(Color.white)
        }
    }
    private func createCalcAC() -> some View {
        return Button(action: {
            self.clearTapped()
        }) {
            (Text("AC"))
                .frame(minWidth: 100, maxWidth: .infinity, minHeight: 20, maxHeight: 20, alignment: .center).font(.headline)
                .foregroundColor(Color.white)
        }
    }
    
    private func digitTapped(_ number: String) -> Void {
        if isTypingNumber {
            if(calculatorText == "0"){
            calculatorText = number
            }else{
               calculatorText += number
            }
        } else {
            calculatorText = number
            isTypingNumber = true
        }
    }
    
    private func dotTapped() -> Void {
        if isTypingDot && !calculatorText.contains("."){
            
            calculatorText += "."
            isTypingDot = false
            isTypingNumber = true
        }
    }
    
    private func minusTapped() {
        minusNumber = (calculatorText as NSString).doubleValue
        result = minusNumber * -1
        calculatorText = NSNumber(value: result).stringValue
    }
    
    private func operandTapped(_ operand: String) {
        isTypingNumber = false
        isTypingDot = true
//        firstNumber = (calculatorText as NSString).doubleValue
        if self.operand != ""{
            self.calculate()
        } else {
            firstNumber = (calculatorText as NSString).doubleValue
        }
        self.operand = operand
    }
    
    private func percentTapped() {
        isTypingNumber = false
        
        
        if (firstNumber == 0.0) {
            let temp = Double(calculatorText) ?? 0.0
            result = temp / 100
            calculatorText = NSNumber(value: result).stringValue
        }else{
            let temp = Double(calculatorText) ?? 0.0
            result = firstNumber * temp / 100
            calculatorText = NSNumber(value: result).stringValue
        }

    }
    
    private func clearTapped(){
        isTypingNumber = false
        isTypingDot = true
        firstNumber = 0
        secondNumber = 0
        result = 0
        operand = ""
        calculatorText = "0"
    }
    private func calculate() {
        isTypingNumber = false
        secondNumber = (calculatorText as NSString).doubleValue
        
        if operand == "+" {
            result = firstNumber + secondNumber
        } else if operand == "-" {
            result = firstNumber - secondNumber
        } else if operand == "*" {
            if(firstNumber == 0 || secondNumber == 0) {
                result = 0
            }else {
                result = firstNumber * secondNumber
            }
            
        } else if operand == "/" {
            result = (firstNumber / secondNumber * 1000).rounded() / 1000
            
        }
        
        
        calculatorText = NSNumber(value: result).stringValue
        firstNumber = (calculatorText as NSString).doubleValue
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

