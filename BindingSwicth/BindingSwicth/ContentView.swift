//
//  ContentView.swift
//  BindingSwicth
//
//  Created by student on 2/12/20.
//  Copyright © 2020 student. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State var showText: Bool=false
    var body: some View {
        VStack{
            if self.showText{
                Text("Hello")
            }
            ChildView(show: $showText)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct ChildView:View {
    @Binding var show:Bool
    var body:some View{
        Button(action:{
            self.show.toggle()
        }){
            Text(self.show ? "hide":"show")
        }
    }
}
