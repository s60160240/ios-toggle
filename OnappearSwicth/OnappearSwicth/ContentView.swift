//
//  ContentView.swift
//  OnappearSwicth
//
//  Created by student on 2/12/20.
//  Copyright © 2020 student. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView{
            VStack{
                NavigationLink(destination: DetailView()){
                    Text("Hello world")
                }
            }
        }.onAppear{
            print("ContentView appeared")
        }.onDisappear{
            print("ContentView disappeared!")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct DetailView:View {
    var body: some View{
        VStack{
            Text("Second View")
        }.onAppear{
            print("DetailView appeared!")
        }.onDisappear{
            print("DetailView disappeared!")
        }
    }
}
