//
//  ContentView.swift
//  TabviewSwift
//
//  Created by student on 3/4/20.
//  Copyright © 2020 student. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        TabView {
            Text("First Visw")
                .tabItem{
                    Image(systemName: "1.circle")
                    Text("First")
            }.tag(0)
            
            Text("SecondView")
                .tabItem{
                    Image(systemName: "2.circle")
                    Text("Second")
            }.tag(1)
            
            Text("SecondView")
                .tabItem{
                    Image(systemName: "2.circle")
                    Text("Second")
            }.tag(2)
            
            Text("SecondView")
                .tabItem{
                    Image(systemName: "2.circle")
                    Text("Second")
            }.tag(3)
            
            Text("SecondView")
                .tabItem{
                    Image(systemName: "2.circle")
                    Text("Second")
            }.tag(4)
            
            Text("SecondView")
                .tabItem{
                    Image(systemName: "2.circle")
                    Text("Second")
            }.tag(5)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
