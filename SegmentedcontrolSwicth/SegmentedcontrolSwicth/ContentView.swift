//
//  ContentView.swift
//  SegmentedcontrolSwicth
//
//  Created by student on 2/5/20.
//  Copyright © 2020 student. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State private var favoriteColor = 0
    var colors = ["Red", "Green", "Blue"]
    var body: some View {
//        VStack{
//            Picker(selection:$favoriteColor, label: Text("What is your favorite color?")){
//                Text("Red").tag(0)
//                Text("Green").tag(1)
//                Text("Blue").tag(2)
//            }.pickerStyle(SegmentedPickerStyle())
//            Text("Value: \(favoriteColor)")
//        }
        VStack{
            Picker(selection:$favoriteColor, label: Text("What is your favorite color?")){
                ForEach(0..<colors.count){ index in
                    Text(self.colors[index]).tag(index)
                }
            }.pickerStyle(SegmentedPickerStyle())
            Text("Value: \(favoriteColor)")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
