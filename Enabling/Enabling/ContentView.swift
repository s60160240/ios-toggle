//
//  ContentView.swift
//  Enabling
//
//  Created by student on 3/4/20.
//  Copyright © 2020 student. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State private var agreedToTerms = false
    var body: some View {
        NavigationView {
            Form {
                Section {
                    Toggle(isOn:$agreedToTerms){
                        Text("Agree to terms and conditions")
                    }
                }
                Section{
                    Button(action:{
                        //show next screen here
                    }){
                        Text("Continue")
                    }.disabled(!agreedToTerms)
            }
            }.navigationBarTitle("Welcome")
    }
}
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
