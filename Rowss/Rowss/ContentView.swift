//
//  ContentView.swift
//  Rowss
//
//  Created by student on 3/4/20.
//  Copyright © 2020 student. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State private var showingAdvancedOptions = false
    @State private var enableLogging = false
    
    var body: some View {
        Form {
            Section {
                Toggle(isOn:$showingAdvancedOptions.animation()) {
                    Text("Show advanced options")
                }
                if showingAdvancedOptions {
                    Toggle(isOn:$enableLogging){
                        Text("Enable loging")
                    }
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
