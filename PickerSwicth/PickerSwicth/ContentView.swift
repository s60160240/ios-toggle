//
//  ContentView.swift
//  PickerSwicth
//
//  Created by student on 2/5/20.
//  Copyright © 2020 student. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var colors = ["Red","Green","Blue","Tartan"]
    @State private var selectedColor = 0
    var body: some View {
        VStack{
            Picker(selection:$selectedColor, label:Text("Please choosa a color")){
                ForEach(0 ..< colors.count){
                    Text(self.colors[$0])
                }
            }.labelsHidden()
            Text("You selected:\(colors[selectedColor])")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
