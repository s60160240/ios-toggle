//
//  ContentView.swift
//  PlacholodertoatextfieldSwicth
//
//  Created by student on 2/5/20.
//  Copyright © 2020 student. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State private var emailAddress = ""
    var body: some View {
        TextField("60160240@go.buu.ac.th", text: $emailAddress).textFieldStyle(RoundedBorderTextFieldStyle()).padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
