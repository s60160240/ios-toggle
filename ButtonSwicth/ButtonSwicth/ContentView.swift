//
//  ContentView.swift
//  ButtonSwicth
//
//  Created by student on 2/5/20.
//  Copyright © 2020 student. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State private var showDetails = false
    var body: some View {
                VStack{
                    Button(action: {
                        self.showDetails.toggle()
                    }){
                        Text("Show details")
                    }
        
                    if showDetails{
                        Text("You should follow me on Twitter: @bew256").font(.largeTitle)
                    }
                }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
