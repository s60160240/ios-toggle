//
//  ContentView.swift
//  CombineSwicth
//
//  Created by student on 2/12/20.
//  Copyright © 2020 student. All rights reserved.
//
import Combine
import SwiftUI

struct ContentView: View {
    @ObservedObject var settings = UserAuthentication()
    var body: some View {
        VStack{
            TextField("Username", text: $settings.username).textFieldStyle(RoundedBorderTextFieldStyle())
            Text("Your username is: \(settings.username)")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

class UserAuthentication: ObservableObject{
    let objectWillChange = ObservableObjectPublisher()
    var username = ""{
        willSet{
            objectWillChange.send()
        }
    }
}
