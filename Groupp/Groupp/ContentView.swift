//
//  ContentView.swift
//  Groupp
//
//  Created by student on 3/4/20.
//  Copyright © 2020 student. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Group {
                Text("Line")
                Text("Line")
                Text("Line")
                Text("Line")
                Text("Line")
                Text("Line")
            }
            Group {
                Text("Line")
                Text("Line")
                Text("Line")
                Text("Line")
                Text("Line")
            }
            
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
