//
//  ContentView.swift
//  Pickersinforms
//
//  Created by student on 3/4/20.
//  Copyright © 2020 student. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var strength = ["Mild", "Medium", "Mature"]
    
    @State private var selectedStength=0
    var body: some View {
        NavigationView {
            Form {
                Section {
                    Picker(selection: $selectedStength, label: Text("Strength")) {
                        ForEach(0..<strength.count){
                            Text(self.strength[$0])
                        }
                    }.pickerStyle(WheelPickerStyle())
                }
            }.navigationBarTitle("Select your cheese")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
