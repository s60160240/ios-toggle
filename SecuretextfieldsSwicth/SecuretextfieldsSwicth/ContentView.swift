//
//  ContentView.swift
//  SecuretextfieldsSwicth
//
//  Created by student on 2/5/20.
//  Copyright © 2020 student. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State private var password:String = ""
    var body: some View {
        VStack{
            SecureField("Enter a password", text: $password)
            Text("You entered:\(password)")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
